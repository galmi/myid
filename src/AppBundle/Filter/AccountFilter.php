<?php

namespace AppBundle\Filter;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 16:02
 */
class AccountFilter extends SQLFilter
{

    /**
     * Gets the SQL query part to add to a query.
     *
     * @param ClassMetaData $targetEntity
     * @param string $targetTableAlias
     *
     * @return string The constraint SQL if there is available, empty string otherwise.
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        $query = '';
        $fieldName = 'account';
        if ($targetEntity->hasAssociation($fieldName)) {
            $query = sprintf('%s.%s = %s', $targetTableAlias, 'account_id', $this->getParameter('account_id'));
        }

        return $query;
    }
}