<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 19/02/2017
 * Time: 01:30
 */

namespace AppBundle\EventListener;


use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RegistrationConfirmListener implements EventSubscriberInterface
{
    private $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
            FOSUserEvents::REGISTRATION_CONFIRMED => 'onRegistrationConfirmed',
            FOSUserEvents::RESETTING_RESET_SUCCESS => 'onRegistrationSuccess'
        );
    }

    public function onRegistrationConfirmed(GetResponseUserEvent $event)
    {
        $url = $this->router->generate('profile');

        $event->setResponse(new RedirectResponse($url));
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        $response = new RedirectResponse($this->router->generate('profile'));
        $event->setResponse($response);
    }
}
