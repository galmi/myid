<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 18/04/2017
 * Time: 08:20
 */

namespace AppBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LastPageListener implements EventSubscriberInterface
{

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => array(array('onKernelResponse', 15))
        ];
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        if ($request->get('_route') && substr($request->get('_route'), 0, 1) != '_') {
            $url = $request->getUri();
            $response = $event->getResponse();
            $now = new \DateTime();
            $now->add(new \DateInterval('P1M'));
            $cookie = new Cookie('_last_page', $url, $now);
            $response->headers->setCookie($cookie);
        }
    }
}