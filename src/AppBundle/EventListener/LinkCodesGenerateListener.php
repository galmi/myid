<?php

namespace AppBundle\EventListener;

use AppBundle\Doctrine\ORM\Id\PinGenerator;
use AppBundle\Doctrine\ORM\Id\UniqueCodeGenerator;
use AppBundle\Entity\Link;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 12/01/2017
 * Time: 01:37
 */
class LinkCodesGenerateListener
{
    /** @var UniqueCodeGenerator */
    private $uniqueCodeGenerator;

    /** @var PinGenerator */
    private $pinGenerator;

    public function __construct(UniqueCodeGenerator $uniqueCodeGenerator, PinGenerator $pinGenerator)
    {
        $this->uniqueCodeGenerator = $uniqueCodeGenerator;
        $this->pinGenerator = $pinGenerator;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $entity = $args->getEntity();

        if ($entity instanceof Link) {
            $entity
                ->setCode($this->uniqueCodeGenerator->generate($em, $entity))
                ->setPin($this->pinGenerator->generate($em, $entity))
            ;
        }
    }
}