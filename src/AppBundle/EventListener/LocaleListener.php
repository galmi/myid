<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 12/02/2017
 * Time: 12:44
 */

namespace AppBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleListener implements EventSubscriberInterface
{

    /** @var string */
    private $defaultLocale;

    private $locale;
    private $allowedLocales;

    public function __construct($defaultLocale = 'en', $allowedLocales = ['en'])
    {
        $this->defaultLocale = $defaultLocale;
        $this->locale = $defaultLocale;
        $this->allowedLocales = $allowedLocales;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => array(array('onKernelRequest', 15)),
            KernelEvents::RESPONSE => array(array('onKernelResponse', 15))
        ];
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        // try to see if the locale has been set as a _locale routing parameter
        if ($locale = $request->attributes->get('_locale')) {
            $this->locale = $locale;
        } else {
            // if no explicit locale has been set on this request, use one from the session
            $preferredLocale = $request->getPreferredLanguage($this->allowedLocales);
            $this->locale = $request->cookies->get('_locale', $preferredLocale);
            $request->setLocale($this->locale);
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $now = new \DateTime();
        $now->add(new \DateInterval('P1M'));
        $cookie = new Cookie('_locale', $this->locale, $now);
        $response->headers->setCookie($cookie);
    }
}