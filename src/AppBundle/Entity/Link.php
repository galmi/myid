<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Link
 *
 * @ORM\Table(name="link", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"code"})
 * }, indexes={
 *     @ORM\Index(columns={"code","pin"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LinkRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="entity_region")
 */
class Link
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="string", length=8)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="AppBundle\Doctrine\ORM\Id\UniqueStringGenerator")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_linked", type="datetime", nullable=true)
     */
    private $date_linked;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = true;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=8, unique=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="pin", type="string", length=4)
     */
    private $pin;

    /**
     * @var Profile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Profile", inversedBy="links")
     * @ORM\JoinColumn(fieldName="profile_id", referencedColumnName="id", nullable=true)
     */
    private $profile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="qr_generated", type="boolean", options={"default": false})
     */
    private $qrGenerated = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="variant", type="integer", nullable=true)
     */
    private $variant;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ViewLog", mappedBy="link")
     */
    private $logs;

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Link
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get pin
     *
     * @return string
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @param string $pin
     * @return $this
     */
    public function setPin($pin)
    {
        $this->pin = $pin;

        return $this;
    }

    /**
     * @param bool $qrGenerated
     * @return Link
     */
    public function setQrGenerated($qrGenerated)
    {
        $this->qrGenerated = $qrGenerated;

        return $this;
    }

    /**
     * @return bool
     */
    public function isQrGenerated()
    {
        return $this->qrGenerated;
    }

    /**
     * @param Profile $profile
     * @return Link
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        $this->date_linked = new \DateTime();

        return $this;
    }

    /**
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param \DateTime $date_linked
     * @return Link
     */
    public function setDateLinked($date_linked)
    {
        $this->date_linked = $date_linked;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateLinked()
    {
        return $this->date_linked;
    }

    /**
     * @param int $variant
     * @return Link
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;

        return $this;
    }

    /**
     * @return int
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @return ArrayCollection
     */
    public function getLogs()
    {
        return $this->logs;
    }
}

