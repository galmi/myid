<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Profile\Sections;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Profile
 *
 * @ORM\Table(name="profile")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProfileRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="entity_region")
 */
class Profile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $date_created;

    /**
     * @var bool
     * @ORM\Column(name="lost_mode", type="boolean", options={"default": false})
     */
    private $lostMode = false;

    /**
     * @var string
     * @ORM\Column(name="one_signal_user_id", type="string", length=255)
     */
    private $oneSignalUserId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", options={"default": true})
     */
    private $active = true;

    /**
     * @var Sections
     *
     * @ORM\Column(name="data", type="object", nullable=true)
     */
    private $data;

    /**
     * @var ArrayCollection<Link>
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Link", mappedBy="profile")
     */
    private $links;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Account", inversedBy="profiles", cascade={"persist"})
     * @ORM\JoinColumn(fieldName="account_id", referencedColumnName="id", nullable=true)
     */
    private $account;

    public function __construct()
    {
        $this->links = new ArrayCollection();
        $this->date_created = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param Sections $data
     *
     * @return Profile
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return Sections
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param bool $active
     * @return Profile
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return ArrayCollection|Link[]
     */
    public function getLinks()
    {
        return $this->links;
    }

    public function addLink(Link $link)
    {
        if (!$this->links->contains($link)) {
            $this->links->add($link);

            $link->setProfile($this);
        }

        return $this;
    }

    public function removeLink(Link $link)
    {
        $this->links->removeElement($link);
        $link->setProfile(null);

        return $this;
    }

    /**
     * @param ArrayCollection $links
     * @return Profile
     */
    public function setLinks($links)
    {
        $this->links = $links;

        return $this;
    }

    /**
     * @param Account $account
     * @return Profile
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param \DateTime $date_created
     * @return Profile
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @return bool
     */
    public function isLostMode()
    {
        return $this->lostMode;
    }

    /**
     * @param bool $lostMode
     * @return Profile
     */
    public function setLostMode($lostMode)
    {
        $this->lostMode = $lostMode;
        return $this;
    }

    /**
     * @return string
     */
    public function getOneSignalUserId()
    {
        return $this->oneSignalUserId;
    }

    /**
     * @param string $oneSignalUserId
     * @return Profile
     */
    public function setOneSignalUserId($oneSignalUserId)
    {
        $this->oneSignalUserId = $oneSignalUserId;
        return $this;
    }

}

