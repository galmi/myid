<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 00:58
 */

namespace AppBundle\Entity\Profile;


class Other
{
    /** @var string */
    public $label;

    /** @var string */
    public $notes;

    /** @var boolean */
    public $public;
}