<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 00:55
 */

namespace AppBundle\Entity\Profile;


class Physician
{
    /** @var string */
    public $name;

    /** @var string */
    public $phone;

    /** @var string */
    public $speciality;

    /** @var string */
    public $notes;

    /** @var boolean */
    public $public;
}