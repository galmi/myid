<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 00:57
 */

namespace AppBundle\Entity\Profile;


class Insurance
{
    /** @var string */
    public $name;

    /** @var string */
    public $idNumber;

    /** @var string */
    public $groupNumber;

    /** @var string */
    public $bin;

    /** @var string */
    public $deductible;

    /** @var string */
    public $phone;

    /** @var string */
    public $notes;

    /** @var boolean */
    public $public;
}