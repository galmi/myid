<?php

namespace AppBundle\Entity\Profile;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Sections
 * @package AppBundle\Entity\Profile
 */
class Sections
{
    /** @var PersonalInformation */
    public $personalInformation;

    /** @var MedicalHighlight[] */
    public $medicalHighlights;

    /** @var string */
    public $additionalKeyInformation;

    /** @var Address */
    public $address;

    /** @var EmergencyContact[] */
    public $emergencyContacts;

    /** @var Medication[]s */
    public $medications;

    /** @var Physician[] */
    public $physicians;

    /** @var Insurance[] */
    public $insurances;

    /** @var Other[] */
    public $others;

    /** @var Document[] */
    public $documents;

    /**
     * @return EmergencyContact[]
     */
    public function getPrimaryContacts()
    {
        $primaryContacts = [];
        foreach ($this->emergencyContacts as $emergencyContact) {
            if ($emergencyContact->primary) {
                $primaryContacts[] = $emergencyContact;
            }
        }
        if (empty($primaryContacts) && !empty($this->emergencyContacts)) {
            $primaryContacts[] = $this->emergencyContacts[0];
        }
        return $primaryContacts;
    }

    /**
     * @return EmergencyContact[]
     */
    public function getAdditionalContacts()
    {
        $contacts = [];
        $primaryContacts = $this->getPrimaryContacts();
        foreach ($this->emergencyContacts as $emergencyContact) {
            if (!in_array($emergencyContact, $primaryContacts)) {
                $contacts[] = $emergencyContact;
            }
        }

        return $contacts;
    }

    /**
     * @return MedicalHighlight[]
     */
    public function getMedicalHighlightByGroups()
    {
        $data = [];
        foreach ($this->medicalHighlights as $medicalHighlight) {
            if (!isset($data[$medicalHighlight->getCategoryLabel()])) {
                $data[$medicalHighlight->getCategoryLabel()] = [];
            }
            $data[$medicalHighlight->getCategoryLabel()][] = $medicalHighlight;
        }

        return $data;
    }
}