<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 00:49
 */

namespace AppBundle\Entity\Profile;


class Address
{
    /** @var string */
    public $type;

    /** @var string */
    public $address;

    /** @var string */
    public $city;

    /** @var string */
    public $zip;

    /** @var string */
    public $state;

    /** @var string */
    public $notes;

    /** @var bool */
    public $public = true;
}