<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 15/01/2017
 * Time: 11:51
 */

namespace AppBundle\Entity\Profile;


class MedicalHighlight
{
    const CATEGORY_TO_LABEL = [
        'allergy' => 'title.allergy',
        'medicalDevices' => 'title.medical_devices',
        'medicalProblems' => 'title.medical_problems',
        'surgery' => 'title.surgery',
    ];

    public static $categories = [
        'title.allergy' => 'allergy',
        'title.medical_devices' => 'medicalDevices',
        'title.medical_problems' => 'medicalProblems',
        'title.surgery' => 'surgery'
    ];

    /** @var string */
    public $category;

    /** @var string */
    public $name;

    /** @var string */
    public $notes;

    /** @var boolean */
    public $public = true;

    public function getCategoryLabel()
    {
        return self::CATEGORY_TO_LABEL[$this->category];
    }
}