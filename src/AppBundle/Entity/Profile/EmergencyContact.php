<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 00:50
 */

namespace AppBundle\Entity\Profile;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class EmergencyContact
 * @package AppBundle\Entity\Profile
 * @ORM\Embeddable()
 */
class EmergencyContact
{
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    public $name;

    /** @var string */
    public $relationship;

    /** @var string */
    public $phone;

    /** @var string */
    public $altPhone;

    /** @var string */
    public $email;

    /** @var bool */
    public $public = true;

    /** @var bool */
    public $primary = true;
}