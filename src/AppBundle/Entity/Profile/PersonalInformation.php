<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 00:41
 */

namespace AppBundle\Entity\Profile;

/**
 * Class PersonalInformation
 * @package AppBundle\Entity\Profile
 */
class PersonalInformation
{
    const GENDER_TO_LABEL = [
        'male' => 'choice.male',
        'female' => 'choice.female'
    ];

    const BLOOD_TO_LABEL = [
        'A+' => 'choice.A_plus',
        'A-' => 'choice.A_minus',
        'B+' => 'choice.B_plus',
        'B-' => 'choice.B_minus',
        'AB+' => 'choice.AB_plus',
        'AB-' => 'choice.AB_minus',
        'O+' => 'choice.O_plus',
        'O-' => 'choice.O_minus'
    ];

    /** @var string */
    public $name;

    /** @var string */
    public $phone;

    /** @var \DateTime */
    public $birthdate;

    /** @var string */
    public $gender;

    /** @var string */
    public $speaks;

    /** @var string */
    public $blood;

    /** @var string */
    public $photo;

    /** @var string */
    public $passport;

    public function getGenderLabel()
    {
        return static::GENDER_TO_LABEL[$this->gender];
    }

    public function getBloodLabel()
    {
        $blood = static::BLOOD_TO_LABEL;

        return isset($blood[$this->blood])?$blood[$this->blood]:'';
    }
}