<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 00:54
 */

namespace AppBundle\Entity\Profile;


class Medication
{
    /** @var string */
    public $name;

    /** @var string */
    public $dosage;

    /** @var string */
    public $strength;

    /** @var string */
    public $frequency;

    /** @var string */
    public $route;

    /** @var string */
    public $notes;

    /** @var boolean */
    public $public;
}