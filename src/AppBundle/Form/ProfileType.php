<?php

namespace AppBundle\Form;

use AppBundle\Entity\Link;
use AppBundle\Entity\Profile;
use AppBundle\Form\Profile\SectionsType;
use AppBundle\Form\Type\PurecssCheckboxType;
use AppBundle\Repository\LinkRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class ProfileType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lostMode')
            ->add('oneSignalUserId', HiddenType::class)
            ->add('data', SectionsType::class, ['label' => false, 'constraints' => array(new Valid())])
//            ->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
//                $data = $event->getData();
//                $form = $event->getForm();
//                if ($data instanceof Profile && empty($data->getAccount())) {
//                    $form
//                        ->add('account', RegistrationType::class, ['required' => false, 'constraints' => array(new Valid())])
//                    ;
//                }
//            });
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile',
            'label' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile';
    }


}
