<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 26/01/2017
 * Time: 11:20
 */

namespace AppBundle\Form\Confirmation;


use AppBundle\Entity\Link;
use AppBundle\Entity\Profile;
use AppBundle\Repository\LinkRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PinType extends AbstractType
{

    /** @var Profile */
    private $profile;

    /** @var LinkRepository */
    private $link;

    public function __construct(LinkRepository $link)
    {
        $this->link = $link;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->profile = $options['profile'];

        $builder
            ->add('link_code', IntegerType::class,['label' => 'label.code', 'mapped' => false])
            ->add('link_pin', IntegerType::class,['label' => 'label.pin', 'mapped' => false])
            ->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
                $form = $event->getForm();
                $linkCode = $form->get('link_code')->getData();
                $linkPin = $form->get('link_pin')->getData();
                /** @var Link $link */
                $link = $this->link->findOneBy([
                    'code' => $linkCode,
                    'pin' => $linkPin
                ]);
                if (empty($link) || (!empty($link->getProfile()) && $link->getProfile() != $this->profile)) {
                    $form->get('link_code')->addError(new FormError('error.id_pin_wrong'));
                }
            });
        ;
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'profile' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_confirmation_pin';
    }

}