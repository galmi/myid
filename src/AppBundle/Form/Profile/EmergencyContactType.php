<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 23:27
 */

namespace AppBundle\Form\Profile;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmergencyContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'label.contact.name'])
            ->add('relationship', TextType::class, ['label' => 'label.contact.relationship', 'required' => false])
            ->add('phone', TextType::class, ['label' => 'label.contact.phone'])
            ->add('altPhone', TextType::class, ['label' => 'label.contact.alt_phone', 'required' => false])
            ->add('email', TextType::class, ['label' => 'label.contact.email', 'required' => false])
//            ->add('public', CheckboxType::class, ['label' => 'label.public', 'required' => false])
            ->add('primary', CheckboxType::class, ['label' => 'label.contact.primary', 'required' => false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile\EmergencyContact',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile_emergency_contact';
    }
}