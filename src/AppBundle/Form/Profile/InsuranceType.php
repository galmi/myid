<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 23:40
 */

namespace AppBundle\Form\Profile;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InsuranceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'label.insurance.name'])
            ->add('idNumber', TextType::class, ['label' => 'label.insurance.id_number', 'required' => false])
            ->add('groupNumber', TextType::class, ['label' => 'label.insurance.group_number', 'required' => false])
            ->add('bin', TextType::class, ['label' => 'label.insurance.bin', 'required' => false])
            ->add('deductible', TextType::class, ['label' => 'label.insurance.deductible', 'required' => false])
            ->add('phone', TextType::class, ['label' => 'label.insurance.phone'])
            ->add('notes', TextareaType::class, ['label' => 'label.insurance.notes', 'required' => false])
//            ->add('public', CheckboxType::class, ['label' => 'label.public', 'required' => false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile\Insurance'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile_insurance';
    }
}