<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 23:35
 */

namespace AppBundle\Form\Profile;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MedicationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'label.medication.name'])
            ->add('dosage', TextType::class, ['label' => 'label.medication.dosage', 'required' => false])
            ->add('strength', TextType::class, ['label' => 'label.medication.strength', 'required' => false])
            ->add('frequency', TextType::class, ['label' => 'label.medication.frequency', 'required' => false])
            ->add('route', TextType::class, ['label' => 'label.medication.route', 'required' => false])
            ->add('notes', TextareaType::class, ['label' => 'label.medication.notes', 'required' => false])
//            ->add('public', CheckboxType::class, ['label' => 'label.public', 'required' => false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile\Medication'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile_medication';
    }
}