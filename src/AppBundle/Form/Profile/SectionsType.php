<?php

namespace AppBundle\Form\Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 01:03
 */
class SectionsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('personalInformation', PersonalInformationType::class)
            ->add('emergencyContacts', CollectionType::class, [
                'label' => false,
                'entry_type' => EmergencyContactType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'attr' => ['class' => 'form-row'],
            ])
            ->add('medicalHighlights', CollectionType::class, [
                'label' => false,
                'entry_type' => MedicalHighlightType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('additionalKeyInformation', TextareaType::class, [
                'label' => 'label.additional_key_information',
                'required' => false
            ])
            ->add('address', CollectionType::class, [
                'label' => false,
                'entry_type' => AddressType::class,
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('medications', CollectionType::class, [
                'entry_type' => MedicationType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('physicians', CollectionType::class, [
                'entry_type' => PhysicianType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('insurances', CollectionType::class, [
                'entry_type' => InsuranceType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('others', CollectionType::class, [
                'entry_type' => OtherType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('documents', CollectionType::class, [
                'entry_type' => DocumentType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile\Sections'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile_sections';
    }

}