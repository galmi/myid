<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 23:21
 */

namespace AppBundle\Form\Profile;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonalInformationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('photo', FileType::class, ['label' => 'label.personal.photo', 'required' => false, 'mapped' => false, 'attr' => ['accept' => 'image/*;capture=camera']])
            ->add('photoData', HiddenType::class, ['required' => false, 'mapped' => false])
            ->add('name', TextType::class, ['label' => 'label.personal.name'])
            ->add('phone', TextType::class, ['label' => 'label.personal.phone', 'required' => false])
            ->add('passport', TextType::class, ['label' => 'label.personal.passport', 'required' => false])
            ->add('birthdate', DateType::class, ['label' => 'label.personal.birthdate', 'widget' => 'single_text', 'required' => false])
            ->add('gender', ChoiceType::class, ['label' => 'label.personal.gender', 'choices' => [
                'choice.male' => 'male',
                'choice.female' => 'female'
            ], 'empty_data' => null, 'placeholder' => 'placeholder.choose_gender', 'required' => false])
            ->add('speaks', TextType::class, ['label' => 'label.personal.speaks', 'required' => false])
            ->add('blood', ChoiceType::class, ['label' => 'label.personal.blood_type', 'choices' => [
                'choice.A_plus' => 'A+',
                'choice.A_minus' => 'A-',
                'choice.B_plus' => 'B+',
                'choice.B_minus' => 'B-',
                'choice.AB_plus' => 'AB+',
                'choice.AB_minus' => 'AB-',
                'choice.O_plus' => 'O+',
                'choice.O_minus' => 'O-'
            ], 'empty_data' => null, 'placeholder' => 'placeholder.choose_blood', 'required' => false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile\PersonalInformation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile_personal_information';
    }
}