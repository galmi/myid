<?php

namespace AppBundle\Form\Profile;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 23:41
 */
class DocumentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label', TextType::class, ['label' => 'label.other.label'])
            ->add('path', HiddenType::class, ['disabled' => true])
            ->add('photo', FileType::class, ['label' => 'label.personal.photo', 'required' => false, 'mapped' => false, 'attr' => ['accept' => 'image/*;capture=camera']])
            ->add('photoData', HiddenType::class, ['required' => false, 'mapped' => false])
//            ->add('public', CheckboxType::class, ['label' => 'label.public', 'required' => false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile\Document'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile_document';
    }
}