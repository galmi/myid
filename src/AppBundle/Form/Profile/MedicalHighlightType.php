<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 23:34
 */

namespace AppBundle\Form\Profile;


use AppBundle\Entity\Profile\MedicalHighlight;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MedicalHighlightType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', ChoiceType::class, [
                'label' => 'label.highlight.category',
                'choices' => MedicalHighlight::$categories,
                'required' => true,
                'placeholder' => 'placeholder.choose_category'
            ])
            ->add('name', TextType::class, ['label' => 'label.highlight.name'])
            ->add('notes', TextareaType::class, ['label' => 'label.highlight.notes', 'required' => false])
//            ->add('public', CheckboxType::class, ['label' => 'label.public', 'required' => false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile\MedicalHighlight',
            'label' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile_medical_highlight';
    }
}