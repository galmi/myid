<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 14/01/2017
 * Time: 21:10
 */

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PurecssCheckboxType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'label' => false
        ]);
    }

    public function getParent()
    {
        return CheckboxType::class;
    }
}