<?php

namespace AppBundle\Command;

use AppBundle\Entity\Link;
use Doctrine\ORM\EntityManager;
use PHPQRCode\Constants;
use PHPQRCode\QRcode;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class GenerateQrCodesCommand extends ContainerAwareCommand
{

    private $variants = array(
        1 => array(
            'code' => array(
                'x' => 66,
                'y' => 305
            ),
            'pin' => array(
                'x' => 82,
                'y' => 334
            ),
            'qr' => array(
                'dst_x' => 7,
                'dst_y' => 42,
                'src_x' => 0,
                'src_y' => 2,
                'src_w' => 222,
                'src_h' => 208,
            )
        ),
        2 => array(
            'code' => array(
                'x' => 56,
                'y' => 254
            ),
            'pin' => array(
                'x' => 74,
                'y' => 283
            ),
            'qr' => array(
                'dst_x' => 46,
                'dst_y' => 46,
                'src_x' => 20,
                'src_y' => 20,
                'src_w' => 145,
                'src_h' => 145,
            )
        )
    );

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:generate_qr_codes')
            ->setDescription('Generating qr code images')
//            ->addOption('size', 's', InputOption::VALUE_REQUIRED, 'Pixels per point size', 6)
            ->addOption('count', 'c', InputOption::VALUE_REQUIRED, 'Count codes', 1)
            ->addOption('id', 'i', InputOption::VALUE_OPTIONAL, 'Code ID')
            ->addOption('variant', 'a', InputOption::VALUE_OPTIONAL, 'Variant ID: 1 - Wristband, 2 - Pet Tag')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = $input->getOption('count');
        $id = $input->getOption('id');
        $variant = $input->getOption('variant');
        $size = $this->getQRSize($variant);//$input->getOption('size');

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        if ($id) {
            $links = $em->getRepository('AppBundle:Link')->findBy(array(
                'id' => $id
            ));
        } else {
            $links = $em->getRepository('AppBundle:Link')->findBy(array(
                'qrGenerated' => 0
            ), null, $count);
        }

        $plateTemplate = $this->getPlateTemplate($variant);
        $fontPath = $this->getContainer()->getParameter('kernel.root_dir').'/Resources/fonts/SukhumvitSet-Text.ttf';
        $opt = $this->variants[$variant];
        /** @var Link $link */
        foreach ($links as $link) {
            $outputFile = $this->getOutputFile($link, $variant);
            QRcode::png($this->getText($link), $outputFile, Constants::QR_ECLEVEL_Q, $size);
            $qrImage = imagecreatefrompng($outputFile);
            $image = imagecreatefrompng($plateTemplate);
            $white = imagecolorallocate($image, 255, 255, 255);
            $black = imagecolorallocate($image, 0, 0, 0);
            imagefill($image, 0, 0, $white);
            imagettftext($image, 22, 0, $opt['code']['x'], $opt['code']['y'], $black, $fontPath, $link->getCode());
            imagettftext($image, 22, 0, $opt['pin']['x'], $opt['pin']['y'], $black, $fontPath, $link->getPin());
            imagecopymerge($image, $qrImage, $opt['qr']['dst_x'], $opt['qr']['dst_y'], $opt['qr']['src_x'], $opt['qr']['src_y'], $opt['qr']['src_w'], $opt['qr']['src_h'], 100);
            imagepng($image, $outputFile);
            $im = new \Imagick();
            $im->readImage($outputFile);
            $im->setImageUnits(\Imagick::RESOLUTION_PIXELSPERINCH);
            $im->setImageResolution(300,300);
            $im->setImageFormat("png");
            $im->writeImage($outputFile);

            $link
                ->setQrGenerated(true)
                ->setVariant($variant)
            ;
            $em->persist($link);
        }

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Confirm QR Code was generated [y] ', false);

        if ($helper->ask($input, $output, $question)) {
            $em->flush();
        }
    }

    private function getText(Link $link)
    {
        return 'http://myid.in.th/' . $link->getId();
    }

    private function getStoreFolder()
    {
        return realpath($this->getContainer()->getParameter('kernel.root_dir') . '/../var/qrcodes');
    }

    private function getOutputFile(Link $link, $variant)
    {
        $folder = $this->getStoreFolder().'/'.$variant;
        @mkdir($folder, 0777, true);
        return $folder. '/' . $link->getId() . '.png';
    }

    private function getPlateTemplate($variant)
    {
        switch ($variant) {
            case 2:
                return $this->getContainer()->getParameter('kernel.root_dir').'/Resources/plate_30x20-pet.png';
                break;
            default:
                return $this->getContainer()->getParameter('kernel.root_dir').'/Resources/plate_30x20.png';
        }
    }

    private function getQRSize($variant)
    {
        switch ($variant) {
            case 2:
                return 5;
                break;
            default:
                return 6;
        }
    }
}
