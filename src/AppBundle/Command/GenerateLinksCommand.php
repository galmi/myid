<?php

namespace AppBundle\Command;

use AppBundle\Entity\Link;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateLinksCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:generate_links')
            ->setDescription('Generate new links')
            ->addOption('count', 'c', InputOption::VALUE_OPTIONAL, 'How many links', 1)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = $input->getOption('count');
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        for ($i = 1; $i <= $count; $i++) {
            $link = new Link();
            $em->persist($link);
            $em->flush($link);
            $em->detach($link);
            unset($link);
        }

        $output->writeln('OK');
    }
}
