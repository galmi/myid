<?php

namespace AppBundle\Doctrine\ORM\Id;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class UniqueCodeGenerator extends AbstractIdGenerator
{
    private $stringLength = 8;
    private $field = 'code';

    /**
     * Generates an identifier for an entity.
     *
     * @param EntityManager|EntityManager $em
     * @param \Doctrine\ORM\Mapping\Entity $entity
     * @return mixed
     */
    public function generate(EntityManager $em, $entity)
    {
        $entityName = $em->getClassMetadata(get_class($entity))->getName();

        do {
            $try = true;
            $id = rand(10000000, 99999999);

            $item = $em->getRepository($entityName)->findOneBy(array(
                $this->field => $id
            ));

            if (!$item) {
                $try = false;
            }

        } while ($try);

        return $id;
    }
}