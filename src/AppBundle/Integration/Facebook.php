<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 01/05/2017
 * Time: 00:30
 */

namespace AppBundle\Integration;


use AppBundle\Entity\Profile;
use AppBundle\Entity\ViewLog;

class Facebook
{
    private $url = 'https://graph.facebook.com/v2.6/me/messages?access_token=';

    /** @var string */
    private $accessToken;

    public function __construct($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function sendMessage(Profile $profile, ViewLog $log)
    {
        $account = $profile->getAccount();
        if (!$account) {
            return false;
        }

        $facebookId = $account->getFacebookId();
        if (!$facebookId) {
            return false;
        }

        $body = $this->getBody($log);
        $url = $this->url . $this->accessToken;
        $ch = curl_init($url);
        $send_data = array(
            'recipient' => array(
                'id' => $facebookId
            ),
            'message' => array(
                'text' => $body
            )
        );
        $jsonDataEncoded = json_encode($send_data);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = @curl_exec($ch);

        return $result;
    }

    private function getBody(ViewLog $log)
    {
        $device = $this->getDevice($log);
        $location = $this->getLocation($log);
        $message = 'Your '.$device.' was scanned. '.$location;

        return $message;
    }

    private function getDevice(ViewLog $log)
    {
        $variant = '';
        switch ($log->getLink()->getVariant()) {
            case 1:
                $variant = 'ID Wristband';
                break;
            case 2:
                $variant = 'Pet ID tag';
                break;
        }

        return $variant;
    }

    private function getLocation(ViewLog $log)
    {
        $message = '';
        if ($log->getLatitude()) {
            $message = "http://maps.google.com/maps?q=loc:{$log->getLatitude()},{$log->getLongitude()}";
        }

        return $message;
    }
}