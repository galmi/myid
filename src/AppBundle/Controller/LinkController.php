<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 19/02/2017
 * Time: 01:39
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Link;
use AppBundle\Entity\Profile;
use AppBundle\Entity\ViewLog;
use AppBundle\Form\Confirmation\PinType;
use AppBundle\Form\ProfileType;
use AppBundle\Form\RegistrationType;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LinkController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|NotFoundHttpException
     */
    public function manualAction(Request $request)
    {
        $id = (string)$request->get('id');
        $pin = (string)$request->get('pin');
        if ($id && $pin) {
            $link = $this->getDoctrine()->getRepository('AppBundle:Link')
                ->findOneBy(array(
                    'code' => $id,
                    'pin' => $pin
                ));
            if ($link) {
                return $this->redirectToRoute('link_view', ['link' => $link->getId()]);
            } else {
                $this->addFlash('error', 'error.id_pin_wrong');
                return $this->redirect($request->headers->get('referer'));
            }
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @param Request $request
     * @param Link $link
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Request $request, Link $link)
    {
        if (!$link->getProfile()) {
            $this->addFlash('error', 'Devices not linked to profile.');
            return $this->redirectToRoute('fos_user_registration_register');
        }

        $log = new ViewLog();
        $log
            ->setLink($link)
            ->setClientIp($request->getClientIp())
            ->setUserAgent($request->headers->get('User-Agent'));
        $this->getDoctrine()->getManager()->persist($log);
        $this->getDoctrine()->getManager()->flush($log);

        return $this->render('link/view.html.twig', [
            'viewLog' => $log,
            'link' => $link,
            'profile' => $link->getProfile(),
            'data' => $link->getProfile()->getData()
        ]);
    }

    public function locationLogAction(Request $request, ViewLog $viewLog)
    {
        if ($request->request->has('latitude') && $request->request->has('longitude')) {
            $viewLog
                ->setLatitude($request->get('latitude'))
                ->setLongitude($request->get('longitude'))
                ;
            $this->getDoctrine()->getManager()->persist($viewLog);
            $this->getDoctrine()->getManager()->flush($viewLog);
        }

        return new JsonResponse(['success' => true]);
    }

    /**
     * @param Request $request
     * @param Link $link
     * @Route("/{link}/new", name="link_new_profile")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Link $link)
    {
        if ($link->getProfile()) {
            return $this->redirectToRoute('link_view', ['link' => $link->getId()]);
        }

        $profile = new Profile();
        $profile->addLink($link);

        $registrationForm = $this->getRegistrationForm($request);
        $confirmationForm = $this->createConfirmationForm($link);
        $form = $this->createForm(ProfileType::class, $profile, [
            'action' => $this->get('router')->generate('link_edit', ['link' => $link->getId()])
        ]);

        return $this->render('link/new.html.twig', [
            'profile' => $profile,
            'form' => $form->createView(),
            'confirmationForm' => $confirmationForm->createView(),
            'registrationForm' => $registrationForm->createView(),
            'link' => $link
        ]);
    }

    /**
     * @param Request $request
     * @param Link $link
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Link $link)
    {
        if ($link->getProfile() && $link->getProfile()->getAccount()) {
            return $this->redirectToRoute('profile_edit', ['profile' => $link->getProfile()->getId()]);
        }
        $confirmationForm = $this->createConfirmationForm($link);
        $confirmationForm->handleRequest($request);
        if ($confirmationForm->isSubmitted() && $confirmationForm->isValid()) {
            $registrationForm = null;
            if (!$link->getProfile() || !$link->getProfile()->getAccount()) {
                $registrationForm = $this->getRegistrationForm($request);
            }

            $em = $this->getDoctrine()->getManager();
            /** @var Profile $profile */
            $profile = $link->getProfile();
            if (empty($profile)) {
                $profile = new Profile();
                $profile->addLink($link);
            } else {
                $data = $profile->getData();
                $profile->setData(clone $data);
            }

            $form = $this->createForm(ProfileType::class, $profile, [
                'action' => $this->get('router')->generate('link_edit', ['link' => $link->getId()])
            ]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $file = $form->get('data')->get('personalInformation')->get('photo')->getData();
                if ($file instanceof UploadedFile) {
                    $fileName = md5(uniqid()).'.'.$file->guessExtension();
                    $photosDirectory = $this->getParameter('photo_directory').$profile->getId();
                    array_map('unlink', glob($photosDirectory.'/*'));
                    $file->move(
                        $photosDirectory,
                        $fileName
                    );
                    $profile->getData()->personalInformation->photo = $profile->getId().'/'.$fileName;
                }
                $em->persist($profile);
                $profile->setData($form->get('data')->getData());

                $success = true;
                $registrationForm = $this->registerUser($request, $registrationForm);
                if ($registrationForm->isSubmitted() &&
                    !empty($registrationForm->get('email')->getData()) &&
                    !empty($registrationForm->get('plainPassword')->getData())
                ) {
                    if ($registrationForm->isValid()) {
                        $account = $registrationForm->getData();
                        $profile->setAccount($account);
                    } else {
                        $success = false;
                        $account = $this->checkLogin($registrationForm);
                        if ($account) {
                            $profile->setAccount($account);
                            $success = true;
                        }
                    }
                }

                $this->getDoctrine()->getManager()->flush();

                if ($success) {
                    return $this->redirectToRoute('link_view', ['link' => $link->getId()]);
                }
            }
            return $this->render('link/new.html.twig', [
                'profile' => $profile,
                'form' => $form->createView(),
                'confirmationForm' => $confirmationForm->createView(),
                'registrationForm' => $registrationForm?$registrationForm->createView():null,
                'link' => $link
            ]);
        }

        return $this->render('link/edit_confirmation.html.twig', [
            'form' => $confirmationForm->createView()
        ]);
    }

    protected function checkLogin(Form $form)
    {
        $email = $form->get('email')->getData();
        $user = $this->get('fos_user.user_provider.username_email')->loadUserByUsername($email);
        if ($user) {
            $password = $form->get('plainPassword')->getData();
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);

            if ($encoder->isPasswordValid($user->getPassword(),$password,$user->getSalt())) {
                return $user;
            }
            $form->get('plainPassword')->addError(new FormError('error.current_password_invalid'));
        }

        return null;
    }

    protected function getRegistrationForm(Request $request)
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm(['required' => false]);
        $form->setData($user);

        return $form;
    }

    protected function registerUser(Request $request, Form $form)
    {
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $form->getData();

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);
        }

        return $form;
    }

    private function createConfirmationForm(Link $link)
    {
        $confirmationData = array();
        $confirmationForm = $this->createForm(PinType::class, $confirmationData, ['profile' => $link->getProfile()]);

        return $confirmationForm;
    }

    public function notifyAction(Request $request, Profile $profile)
    {
        $content = array(
            "en" => $request->get('message')
        );

        $fields = array(
            'app_id' => $this->getParameter('onesignal_app_id'),
            'include_player_ids' => array($profile->getOneSignalUserId()),
            'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return new JsonResponse(['success' => true, 'response' => $response]);
    }

}