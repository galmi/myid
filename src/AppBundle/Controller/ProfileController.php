<?php
/**
 * Created by PhpStorm.
 * User: ildar
 * Date: 19/02/2017
 * Time: 01:02
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Account;
use AppBundle\Entity\Link;
use AppBundle\Entity\Profile;
use AppBundle\Entity\ViewLog;
use AppBundle\Form\LinkType;
use AppBundle\Form\Profile\DocumentType;
use AppBundle\Form\ProfileType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProfileController extends Controller
{

    public function __construct(ContainerInterface $container, EntityManager $em, TokenStorageInterface $tokenStorage)
    {
        $this->container = $container;
        $token = $tokenStorage->getToken();
        $user = $token->getUser();
        $em->getFilters()->enable('account_filter');
        $em->getFilters()->getFilter('account_filter')->setParameter('account_id', $user->getId());
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** @var Account $user */
        $user = $this->getUser();

        $profiles = $user->getProfiles();

        if ($profiles->count() == 0) {
            return $this->redirectToRoute('profile_new');
        }

        return $this->render('profile/index.html.twig', [
            'profiles' => $profiles
        ]);
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $profile = new Profile();
        $profile->setAccount($this->getUser());

        $form = $this->createForm(ProfileType::class, $profile);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($profile);
            $this->getDoctrine()->getManager()->flush($profile);

            $this->processImage($form, $profile);
            $this->processDocuments($form, $profile);
            $profile->setData(clone $profile->getData());
            $profile->setAccount($this->getUser());
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Profile was updated');

            return $this->redirectToRoute('profile');
        }
        return $this->render('link/new.html.twig', [
            'profile' => $profile,
            'form' => $form->createView(),
        ]);
    }

    public function updateAction(Request $request, Profile $profile)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Profile $profile */
        $data = $profile->getData();
        $profile->setData(clone $data);

        $form = $this->createForm(ProfileType::class, $profile);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->processImage($form, $profile);
            $this->processDocuments($form, $profile);
            $em->persist($profile);
            $profile->setData(clone $profile->getData());
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Profile was updated');

            return $this->redirectToRoute('profile');
        }
        return $this->render('link/new.html.twig', [
            'profile' => $profile,
            'form' => $form->createView(),
        ]);
    }

    private function processImage(Form $form, Profile $profile)
    {
        $file = $form->get('data')->get('personalInformation')->get('photo')->getData();
        $photoData = $form->get('data')->get('personalInformation')->get('photoData')->getData();
        $photosDirectory = $profile->getId() . '/personal';
        $photosAbsoluteDirectory = $this->getParameter('photo_directory').$photosDirectory;
        if ($file instanceof UploadedFile) {
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            @mkdir($photosAbsoluteDirectory, 0777, true);
            array_map('unlink', glob($photosAbsoluteDirectory.'/*'));
            $file->move(
                $photosAbsoluteDirectory,
                $fileName
            );
            $this->resizeImage($photosAbsoluteDirectory.'/'.$fileName);
            $profile->getData()->personalInformation->photo = $photosDirectory.'/'.$fileName;
        } elseif (!empty($photoData)) {
            list($type, $photoData) = explode(';', $photoData);
            list(,$extension) = explode('/',$type);
            list(,$photoData)      = explode(',', $photoData);
            $fileName = md5(uniqid()).'.'.$extension;
            $photoData = base64_decode($photoData);
            @mkdir($photosAbsoluteDirectory, 0777, true);
            array_map('unlink', glob($photosAbsoluteDirectory.'/*'));
            file_put_contents($photosAbsoluteDirectory.'/'.$fileName, $photoData);
            $profile->getData()->personalInformation->photo = $photosDirectory.'/'.$fileName;
        }
    }

    private function processDocuments(Form $form, Profile $profile)
    {
        /** @var DocumentType[] $documents */
        $documents = $form->get('data')->get('documents');
        foreach ($documents as $i => $document) {
            $file = $document->get('photo')->getData();
            $photoData = $document->get('photoData')->getData();
            $photosDirectory = $profile->getId().'/documents';
            $photosAbsoluteDirectory = $this->getParameter('photo_directory').$photosDirectory;
            if ($file instanceof UploadedFile) {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                @mkdir($photosAbsoluteDirectory, 0777, true);
                $file->move(
                    $photosAbsoluteDirectory,
                    $fileName
                );
                $this->resizeImage($photosAbsoluteDirectory.'/'.$fileName);
                $profile->getData()->documents[$i]->path = $photosDirectory.'/'.$fileName;
            } elseif (!empty($photoData)) {
                list($type, $photoData) = explode(';', $photoData);
                list(,$extension) = explode('/',$type);
                list(,$photoData)      = explode(',', $photoData);
                $fileName = md5(uniqid()).'.'.$extension;
                $photoData = base64_decode($photoData);
                @mkdir($photosAbsoluteDirectory, 0777, true);
                file_put_contents($photosAbsoluteDirectory.'/'.$fileName, $photoData);
                $profile->getData()->documents[$i]->path = $photosDirectory.'/'.$fileName;
            }
        }
    }

    private function resizeImage($path)
    {
        $maxWidth  = 350;
        $maxHeight = 350;

        $size = getimagesize($path);
        if ($size) {
            $imageWidth  = $size[0];
            $imageHeight = $size[1];
            $wRatio = $imageWidth / $maxWidth;
            $hRatio = $imageHeight / $maxHeight;
            $maxRatio = max($wRatio, $hRatio);
            if ($maxRatio > 1) {
                $outputWidth = $imageWidth / $maxRatio;
                $outputHeight = $imageHeight / $maxRatio;
            } else {
                $outputWidth = $imageWidth;
                $outputHeight = $imageHeight;
            }

            $image = new \Imagick(realpath($path));
            $image->resizeImage($outputWidth, $outputHeight, \Imagick::FILTER_LANCZOS, 1);
            $image->writeImage($path);
            $image->destroy();
        }
    }

    public function linksAction(Profile $profile)
    {
        return $this->render('profile/links.html.twig', [
            'profile' => $profile
        ]);
    }

    public function addLinkAction(Request $request, Profile $profile)
    {
        $link = new Link();
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $realLink = $this->getDoctrine()->getRepository('AppBundle:Link')
                ->findOneBy(array(
                    'code' => $link->getCode(),
                    'pin' => $link->getPin(),
                    'active' => true
                ));
            if ($realLink) {
                if ($realLink->getProfile()) {
                    $this->addFlash('error', 'error.device_linked');
                } else {
                    $realLink->setProfile($profile);
                    /** @var EntityManager $em */
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($realLink);
                    $em->flush($realLink);
                    $this->addFlash('notice', 'info.device_linked');
                }
            } else {
                $this->addFlash('error', 'error.id_pin_wrong');
            }

            return $this->redirectToRoute('profile_links', ['profile' => $profile->getId()]);
        }
        return $this->render('profile/add_link.html.twig', [
            'form' => $form->createView(),
            'profile' => $profile
        ]);
    }

    public function unlinkAction(Request $request, Profile $profile, Link $link)
    {
        $profile->removeLink($link);
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $em->persist($profile);
        $em->flush();
        $this->addFlash('notice', 'info.device_unlinked');
        if ($profile->getLinks()->count() == 0) {
            return $this->redirectToRoute('profile');
        }
        return $this->redirectToRoute('profile_links', ['profile' => $profile->getId()]);
    }

    public function deleteAction(Request $request, Profile $profile)
    {
        $em = $this->getDoctrine()->getManager();
        $profile->setActive(false);
        if ($profile->getLinks()->count() > 0) {
            $this->addFlash('notice', 'info.profile_devices_unlinked');
        }
        foreach ($profile->getLinks() as $link) {
            $profile->removeLink($link);
            $em->persist($link);
        }
        $em->persist($profile);
        $em->flush();

        if ($profile->getId()) {
            $photosDirectory = $this->getParameter('photo_directory').$profile->getId();
            array_map('unlink', glob($photosDirectory.'/*'));
            @rmdir($photosDirectory);
        }

        $this->addFlash('notice', 'info.profile_deleted');
        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    public function previewAction(Profile $profile)
    {
        if (!$profile) {
            throw new NotFoundHttpException();
        }

        return $this->render('link/view.html.twig', [
            'profile' => $profile,
            'data' => $profile->getData()
        ]);
    }

    public function logsAction(Profile $profile)
    {
        /** @var ViewLog[] $logs */
        $logs = $this->getDoctrine()->getRepository('AppBundle:ViewLog')->getLog($profile);

        return $this->render('link/logs.html.twig', [
            'logs' => $logs
        ]);
    }
}