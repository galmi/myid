<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocaleAction(Request $request)
    {
        $redirect = $request->server->get('HTTP_REFERER');
        if (empty($redirect)) {
            $redirect = $request->cookies->get('_last_page');
        }
        if (empty($redirect)) {
            $redirect = $this->get('router')->generate('homepage');
        }

        return $this->redirect($redirect);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function faqAction(Request $request)
    {
        return $this->render('default/faq.html.twig');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sizingAction(Request $request)
    {
        return $this->render('default/sizing.html.twig');
    }

    public function privacyAction(Request $request)
    {
        return $this->render('default/privacy.html.twig');
    }

    public function termsAction(Request $request)
    {
        return $this->render('default/terms.html.twig');
    }
}
